#!/usr/bin/env python
# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#       This example shows a simple unfolding scenario. Truth data is generated
#       according to a bimodal p.d.f. and then simulates detector response on
#       these events in a naive way by applying a simple smearing and 
#       efficiency function. The resulting truth and reco level events are
#       then used to build a response matrix that can be used for unfolding.
#       The to-be unfolded data is simulated by Poisson smearing a disjoint
#       reco level dataset. This data is then unfolded with a unfolding method
#       of choice and, in case of regularization, a preset set of regularization
#       strengths.
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================


dict_data = {}        

# ======= Constants ======= #

# Kinematic ranges. (delta eta)
dict_data['xmin'] = -4         
dict_data['xmax'] = 4          
                      
# Smearing parameters.
dict_data['bias'] = 0.2
dict_data['sigma'] = 0.85

# Efficiency type
dict_data['eff'] = 'lin'
                         
# Bimodal assymmetry.
dict_data['frac'] = 0.4  
                                                  
# Overflow flag.         
overflow = True          
                                                        
# Number of events.            
dict_data['nevents'] = 5000    
                                       
# Constant background bin count        
dict_data['nbkg'] = 0

# ======================== #



# ===== User-defined ===== #
                      
# Truth/Reco bin count.  
dict_data['tbins'] = 20  
dict_data['rbins'] = 20  
                               
# How much more MC do we have than data
dict_data['mcLumiFactor'] = 10         

# ======================== #


from helpers import *


def main(args):

  import ROOT

  # # Prepare the histograms and response matrix.
  histograms = prepare_bimodal(dict_data)

  # Plot the input distributions.
  #plot_input(histograms["truth_train"], histograms["reco_train"], histograms["data"], histograms["response"], histograms["purity"], histograms["eff"])  
  
  # Set the unfolding method and corresponding
  # regularization parameter values. See helpers.py
  # for the actual values.
  alg = algorithm(args.method)

  # Get the truth distribution for training.
  train_truth = histograms["truth_train"]
  
  # Get the reco distribution for training.
  train_reco = histograms["reco_train"]
  
  # Get the to-be unfolded data distribution.
  data = histograms["data"]

  # Get the response matrix.
  response = histograms["response"]

  # Set the regularization parameter.
  regparm = args.regparm
  
  print('Unfolding with the '+str(args.method)+r'-method and regularization strength of '+str(regparm))
  
  # Create a spectator object.  
  spec = ROOT.RooUnfoldSpec("unfold","unfold",train_truth,"obs_truth",train_reco,"obs_reco",response,0,data,overflow,0.0005)
  if not regparm == None:
    func = spec.makeFunc(alg, regparm)
  else:
    func = spec.makeFunc(alg)

  # Create a RooFitHist object as input for the unfolding
  # results printing.
  test_truth_RFH = spec.makeHistogram(histograms["truth_test"])
  
  # Print the unfolding results and compare to a truth histogram.
  func.unfolding().PrintTable(ROOT.cout, test_truth_RFH)

  # Plot the results.
  plot_result(histograms["truth_test"], func, True)


if __name__=="__main__":
  from argparse import ArgumentParser
  parser = ArgumentParser(description="RooUnfold testing script")
  parser.add_argument("method",default="inv",type=str)
  parser.add_argument("--regparm",type=float)
  main(parser.parse_args())
  
