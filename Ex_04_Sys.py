#!/usr/bin/env python
# ==============================================================================
#  File and Version Information:
#       $Id$
#
#  Description:
#       This example loads histograms at truth and reco level evaluated with
#       a variety of nuisance parameter(NP) values to model systematic variation.
#       These histograms are used as input for the unfolding to show the effect
#       of systematic uncertainties on the unfolding result.
#
#  Author: Pim Verschuuren <pim.verschuuren@rhul.ac.uk>
#
# ==============================================================================


from helpers import *


def main(args):

  import ROOT

  # Load the histograms
  histograms = get_histograms("data/histograms_sys.root")

  # Create a spectator object.
  spec = ROOT.RooUnfoldSpec("unfold","unfold",histograms["truth_nom"],"obs_truth",histograms["reco_nom"],"obs_reco",histograms["response_nom"],0,histograms["data"],True,0.0005)  

  # Get the unfolding algorithm.
  alg = algorithm(args.method)

  # Add the systematics.
  spec.registerSystematic(ROOT.RooUnfoldSpec.kTruth, "theorynp", histograms["truth_up"], histograms["truth_down"])
  spec.registerSystematic(ROOT.RooUnfoldSpec.kMeasured, "reconp", histograms["reco_up"], histograms["reco_down"])
  spec.registerSystematic(ROOT.RooUnfoldSpec.kResponse, "respreconp", histograms["response_nomup"], histograms["response_nomdown"])
  spec.registerSystematic(ROOT.RooUnfoldSpec.kResponse, "resptheorynp", histograms["response_downnom"], histograms["response_upnom"])

  if not args.regparm == None:
    func = spec.makeFunc(alg, args.regparm)
  else:
    func = spec.makeFunc(alg)

  # Create a RooFitHist object as input for the unfolding
  # results printing.
  test_truth_RFH = spec.makeHistogram(histograms["truth_test"])
  
  # Get the unfolding object.
  unfold = func.unfolding()

  # Switch on the systematics.
  unfold.IncludeSystematics()

  # Initiate the unfolding by requesting the
  # table with unfolded results.

  # Print results with only stat. errors.
  func.unfolding().PrintTable(ROOT.cout, test_truth_RFH, ROOT.RooUnfolding.kErrors)
  
  # Print results with both stat. and syst. errors.
  func.unfolding().PrintTable(ROOT.cout, test_truth_RFH, ROOT.RooUnfolding.kRooFit)

  # Plot the results.
  plot_result(histograms["truth_test"], func, True)
  

if __name__=="__main__":
  from argparse import ArgumentParser
  parser = ArgumentParser(description="RooUnfold testing script")
  parser.add_argument("method",default="inv",type=str)
  parser.add_argument("--regparm",type=float)
  main(parser.parse_args())
  
